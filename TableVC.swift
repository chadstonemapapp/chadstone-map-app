//
//  BTableViewController.swift
//  Chaddy Map
//
//  Created by Shanya & Ahthithan on 05/05/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/* This view controller shows the table view for
 the side bar menu */

/* It contains a link to all the view controllers 
 in the app and has a customized view*/


import Foundation
import UIKit

class TableVC: UITableViewController {
    
        //Declaring objects required for the table array
        var TableArray = [String]()
    
    override func viewDidLoad() {
        
        //Table array objects
        TableArray = ["Map","Stores","Tutorial","Contact Us"]
        
        //Customizing the view
        self.view.backgroundColor = UIColor(red:0.15, green:0.36, blue:0.62, alpha:1.0)
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return TableArray.count
        return TableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableArray[indexPath.row], for: indexPath as IndexPath) as UITableViewCell
        
        cell.textLabel?.text = TableArray[indexPath.row]
        //cell.textLabel?.textColor = UIColor(red:0.20, green:0.60, blue:0.40, alpha:1.0)
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = UIColor.clear
        //cell.backgroundColor = UIColor(red:0.14, green:0.13, blue:0.13, alpha:1.0)
        //cell.layer.shadowColor = UIColor.gray.cgColor

    
        return cell
        
        
        
    }
   
    

        
        
    }
 
    
    
    
    
    
    
    
    
    
    
    
    

