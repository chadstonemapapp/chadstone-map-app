//
//  ViewController.swift
//  Chaddy Map
//
//  Created by AHTHITHAN SELVANAYAGAM on 02/04/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/*  This view controller displays the categories of the 
    stores available on a tableview */

/*  Firebase is used as the database to store the information
    of all the stores */

/* Includes a search bar to search for shop categories */

import UIKit
import FirebaseDatabase

class CategoriesViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    //Outlets created for the menu Bar Button, Search Bar and Table View.
    @IBOutlet weak var menu: UIBarButtonItem!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    @IBOutlet weak var shopTableView: UITableView!
    
    /*  Initialised the variables to store the data from the
        database as an array */
    var myList:[String] = []
    var categoryToPass:String!
    var filteredData:[String] = []
    
    //Variables initialised to access a location from the database.
    var handle:FIRDatabaseHandle?
    var ref:FIRDatabaseReference?
    
    
    //Setting up the Table View for the application
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myList.count
    }
    
    //Data from the database is entered into each row of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as UITableViewCell
        cell.textLabel?.text = myList[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.textLabel?.textColor = UIColor(red: 0.149, green: 0.3608, blue: 0.6196, alpha: 1.0)
        return cell
    }
    
    //When a category is selected a segue is performed to go to the next page
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // Get Cell Label
        let currentCell = tableView.cellForRow(at: indexPath) as UITableViewCell!;
        categoryToPass = currentCell?.textLabel?.text
        performSegue(withIdentifier: "shopsegue", sender: self)
    }
    
    //This method passes the value selected by the user to the ShopViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "shopsegue") {
            
            //New view controller initialized and casted as next view controller
            let viewController = segue.destination as! ShopViewController
            //New view controller with the property that stores the passed value
            viewController.passedCategory = categoryToPass
        }
    }
    
    /* This idea for the search bar is from a user, Charlie Hieger at https://github.com/codepath/ios_guides/wiki/Search-Bar-Guide */
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        myList = searchText.isEmpty ? myList : myList.filter { (item: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil            
        }
        shopTableView.reloadData()
    }
    /* End user's idea */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
        shopTableView.dataSource = self
        searchBar.delegate = self
        filteredData = myList
        
        // Access the database
        ref = FIRDatabase.database().reference().child("shop")
        
        //Load the data from the database into the table view accordingly
        ref?.queryOrdered(byChild: "bank").observe(.childAdded, with: { snapshot in
            let item = snapshot.key
            self.myList.append(item)
            self.shopTableView.reloadData()
        })
        //This allows the menu button click to open the side bar menu
        menu.target = self.revealViewController()
        menu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        //Gesture for swiping right and opening the side bar menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

