//
//  ShopDetailsViewController.swift
//  Chaddy Map
//
//  Created by AHTHITHAN SELVANAYAGAM on 12/05/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/*  This view controller is used to display the details
    of a particular store selected by the user
 in a table view*/

import UIKit
import FirebaseDatabase
import FirebaseStorage

class ShopDetailsViewController: UIViewController {
    
    // Initialize the variables
    var handle:FIRDatabaseHandle?
    var ref:FIRDatabaseReference?
    var passedCategory = ""
    var passedShop = ""
    var shopDetails:[String] = []
    var shopName = ""
    var websiteToPass:String!

    @IBOutlet weak var shopImage: UIImageView!
    
    @IBOutlet weak var timeMon: UITextView!
    @IBOutlet weak var timeTues: UITextView!
    @IBOutlet weak var timeWed: UITextView!
    @IBOutlet weak var timeThurs: UITextView!
    @IBOutlet weak var timeFri: UITextView!
    @IBOutlet weak var timeSat: UITextView!
    @IBOutlet weak var timeSun: UITextView!
    
    @IBOutlet weak var textbox1: UITextView!
    @IBOutlet weak var textbox2: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        // Do any additional setup after loading the view.
        
        
        // Access the database according the category and shop selected by the user 
        ref = FIRDatabase.database().reference()
        ref?.child("shop").child(passedCategory).child(passedShop).observeSingleEvent(of: .value, with: { (snapshot) in

            let value = snapshot.value as? NSDictionary
            let Name = value?["Name"] as? String ?? ""
            let shopNumber = value?["Shop Number"] as? String ?? ""
            let Time1 = value?["Time1"] as? String ?? ""
            let Time2 = value?["Time2"] as? String ?? ""
            let Time3 = value?["Time3"] as? String ?? ""
            let Website = value?["Website"] as? String ?? ""
            
            // Store the name of the store into a variable.
            self.shopName = Name
            let shopExt = ".png"
            
            // The images of the shop is retrieved from the database displayed accordingly.
            let imageName = self.shopName+shopExt
            let imageURL = FIRStorage.storage().reference(forURL: "gs://chaddymap.appspot.com").child(imageName)
            
            imageURL.downloadURL(completion: { (url, error) in
                if error != nil {
                    print(error?.localizedDescription)
                    return
                }
                
                URLSession.shared.dataTask(with: url!, completionHandler: { (data, response, error) in
                    if error != nil {
                        print(error)
                        return
                    }
                    guard let imageData = UIImage(data: data!) else { return }
                    
                    DispatchQueue.main.async {
                        self.shopImage.image = imageData
                    }
                }).resume()
            })
            
            /*  The timings of the shop are retrived from the database and displayed in the
                landing page of the store accordingly */
            self.timeMon.text = Time1
            self.timeTues.text = Time1
            self.timeWed.text = Time1
            self.timeThurs.text = Time2
            self.timeFri.text = Time2
            self.timeSat.text = Time2
            self.timeSun.text = Time3
            
            self.websiteToPass = Website
            // ...
        }) { (error) in
            print(error.localizedDescription)
        }
        
        //Customzing the view
        textbox1.layer.cornerRadius = 15.0
        textbox2.layer.cornerRadius = 15.0
        self.view.backgroundColor = UIColor.black

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "websegue") {
            
            // initialize new view controller and cast it as your view controller
            let viewController = segue.destination as! WebViewController
            // your new view controller should have property that will store passed value
            viewController.passedShop = passedShop
            viewController.passedCategory = passedCategory
        }
    }

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


