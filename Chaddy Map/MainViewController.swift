//
//  MapViewController.swift
//  Chaddy Map
//
//  Created by SHANYA VAIRAWANATHAN on 3/05/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/* This view controller includes the main 
 screen of the app */

/* It displays images of chadstone using 
 a segment view and image view */

/*  */

import Foundation
import UIKit
import MapKit
import CoreLocation

class MainViewController: UIViewController {
    
    //Declaring the outlets used in the screen
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var textbox1: UITextView!
    @IBOutlet weak var textbox2: UITextView!
    @IBOutlet weak var textbox3: UITextView!
    var imageArray = [UIImage]()
    
    
    //The images in the second scene being controlled by an outlet
    @IBOutlet var slideSegment: UISegmentedControl!
    
    
    //Action for the segment control to display images when each segment is clicked.
    @IBAction func slideShow(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex) {
        //Displays the first image which is the ceremony
        case 0:
            imageView.animationDuration = 50
            imageView.startAnimating()
            imageView.image = UIImage(named:"chadstone1.jpg")
            imageView.image = imageArray [0]


            break;
        // Displays the second image which is honey
        case 1:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone2.jpg")


            break;
        //Displays the third image which is the family life
        case 2:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone3.jpg")

            break;
        //Displays the image in the form of a slide show with a speed of 2
        case 3:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone4.jpg")

            break;
        case 4:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone5.jpg")

            
            break;
        //Displays the third image which is the family life
        case 5:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone6.jpg")

            break;
        case 6:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone7.jpg")

            
            break;
        //Displays the third image which is the family life
        case 7:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone8.jpg")

            break;
        case 8:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone9.jpg")
 
            
            break;
        //Displays the third image which is the family life
        case 9:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone10.jpg")

            break;

            
        default: break;
        }
        
    }

    


    override func viewDidLoad() {
        super.viewDidLoad()

        //Allows the menu bar to be clicked and opens the side bar menu
        menu.target = self.revealViewController()
        menu.action = #selector(SWRevealViewController.revealToggle(_:))
    

        //Customized the view
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.view.backgroundColor = UIColor.black
        
        //Gesture which allows user to swipe right and open side bar menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        
        //Loop for the animation or slide show
        for i in 1...10 {
            let imageName = "chadstone"+"\(i)"+".jpg"
            let image = UIImage(named: imageName)
            imageArray += [image!]
            
        }
        
        //Declaring the array of images which have to be animated
        imageView.animationImages = imageArray
        imageView.image = imageArray [0]
        imageView.contentMode = .scaleAspectFit
    
        
        
        imageView.layer.cornerRadius = 20
        textbox1.layer.cornerRadius = 15.0
        textbox2.layer.cornerRadius = 15.0
        textbox3.layer.cornerRadius = 15.0
        
        UIView.transition(with: self.imageView,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {
                        //self.imageView.image = imageView.animationImages
        },
                          completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    
    
}
