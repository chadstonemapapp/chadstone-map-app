//
//  EmergencyViewController.swift
//  Chaddy Map
//
//  Created by SHANYA VAIRAWANATHAN on 4/04/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/* This allows the user to view the contact
 details of chadstone shopping centre */

/* Includes a call function where the user has
 to click on the button and the app opens the phone
 app of the iphone and contacts chadstone*/

import UIKit

class ContactUsViewController: UIViewController {
    
    //Declaring the outlets for the screen
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var textbox1: UITextView!
    @IBOutlet weak var textbox2: UITextView!
    @IBOutlet weak var textbox3: UITextView!
    @IBOutlet weak var textbox4: UITextView!
    @IBOutlet weak var textbox5: UITextView!
    @IBOutlet weak var textbox6: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Customizing the view
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        self.view.backgroundColor = UIColor.black
        
        //Allows the user to click on the menu button to open the side bar menu
        menu.target = self.revealViewController()
        menu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        //Gesture which allows the user to swipe right and open the side bar menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //Customizing the textview outlets
        textbox1.layer.cornerRadius = 15.0
        textbox2.layer.cornerRadius = 15.0
        textbox3.layer.cornerRadius = 15.0
        textbox4.layer.cornerRadius = 15.0
        textbox5.layer.cornerRadius = 15.0
        textbox6.layer.cornerRadius = 15.0
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Contacts chadstone shopping centre on button click
    @IBAction func makeCall1(_ sender: Any) {

        let url = NSURL(string: "tel://(03)95633355")!
        UIApplication.shared.openURL(url as URL)
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
