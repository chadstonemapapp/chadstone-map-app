//
//  WebViewController.swift
//  Chaddy Map
//
//  Created by SHANYA VAIRAWANATHAN on 18/05/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/*  This view controller is to display the website of the store 
    selected by the user using a webview */

import UIKit
import FirebaseDatabase


class WebViewController: UIViewController {

    // Initialize the necessary handler and variables
    var handle:FIRDatabaseHandle?
    var ref:FIRDatabaseReference?
    var passedCategory = ""
    var passedShop = ""
    var linkToWebsite = ""
    var passedWebsite = ""
    
    @IBOutlet weak var viewWebsite: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Access the database for the website key of the store
        ref = FIRDatabase.database().reference()
        ref?.child("shop").child(passedCategory).child(passedShop).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            let website = value?["Website"] as? String ?? ""
            
            //Store the value of the website to a variable and display in the webview.
            self.linkToWebsite = website
            let url = URL (string : self.linkToWebsite)
            let load = URLRequest(url: url!);
            self.viewWebsite.loadRequest(load)
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
