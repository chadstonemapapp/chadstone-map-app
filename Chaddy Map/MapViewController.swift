//
//  MapViewController.swift
//  Chaddy Map
//
//  Created by SHANYA VAIRAWANATHAN on 3/05/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.

//
/* This view controller opens up apple maps 
 for navigation */

/* It includes the route to chadstone shopping centre
 if you are not at chadstone and it also shows the 
 routes to each store if the user is in chastone*/

import Foundation
import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate {
    
    //Declaring the outlets for the screen
    @IBOutlet weak var menu: UIBarButtonItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var textbox1: UITextView!
    @IBOutlet weak var textbox2: UITextView!
    @IBOutlet weak var textbox3: UITextView!
    var imageArray = [UIImage]()
    
    
    //The images in the second scene being controlled by an outlet
    @IBOutlet var slideSegment: UISegmentedControl!


    //Action for the segment control to display images when each segment is clicked.
    @IBAction func slideShow(_ sender: UISegmentedControl) {
        switch (sender.selectedSegmentIndex) {
        //Displays the first image
        case 0:
            imageView.animationDuration = 50
            imageView.startAnimating()
            imageView.image = UIImage(named:"chadstone1.jpg")
            imageView.image = imageArray [0]
            
            
            break;
        // Displays the second image
        case 1:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone2.jpg")
            
            
            break;
        //Displays the third image which
        case 2:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone3.jpg")
            
            break;
        //Displays the fourth image
        case 3:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone4.jpg")
            
            break;
        //Displays the fifth image
        case 4:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone5.jpg")
            
            
            break;
        //Displays the sixth image
        case 5:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone6.jpg")
            
            break;
            //Displays the seventh image
        case 6:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone7.jpg")
            
            
            break;
        //Displays the eigth image
        case 7:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone8.jpg")
            
            break;
            //Displays the ninth image
        case 8:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone9.jpg")
            
            
            break;
        //Displays the tenth image
        case 9:
            imageView.stopAnimating()
            imageView.image = UIImage(named:"chadstone10.jpg")
            
            break;
            
            
        default: break;
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Launches apple maps
        openMapForPlace()

        //This allows the menu button click to open the side bar menu
        menu.target = self.revealViewController()
        menu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        //Allows the user to swipe right to open the side bar menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        self.imageView.animationImages = [
            UIImage(named: "chadstone1.jpg")!,
            UIImage(named: "chadstone2.jpg")!,
            UIImage(named: "chadstone3.jpg")!,
            UIImage(named: "chadstone4.jpg")!,
            UIImage(named: "chadstone5.jpg")!,
            UIImage(named: "chadstone6.jpg")!,
            UIImage(named: "chadstone7.jpg")!,
            UIImage(named: "chadstone8.jpg")!,
            UIImage(named: "chadstone9.jpg")!,
            UIImage(named: "chadstone10.jpg")!
        ]
        
        
        self.imageView.animationDuration = 50.0
        self.imageView.startAnimating()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        imageView.layer.cornerRadius = 20
        textbox1.layer.cornerRadius = 15.0
        textbox2.layer.cornerRadius = 15.0
        textbox3.layer.cornerRadius = 15.0
        self.view.backgroundColor = UIColor.black
        UIView.transition(with: self.imageView,
                          duration: 0.3,
                          options: .transitionFlipFromBottom,
                          animations: {
                            //self.imageView.image = imageView.animationImages
        },
                          completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //This function sets the location for chadstone shopping centre
    //It integrated apple maps to be launched
    func openMapForPlace() {
        
        
        
        let latitude: CLLocationDegrees = -37.884829794
        let longitude: CLLocationDegrees = 145.076166362
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Chadstone Shopping Center"
        mapItem.openInMaps(launchOptions: options)
        //map.setRegion(regionSpan, animated: true)
    }

    

    
    
}
