//
//  ShopViewController.swift
//  Chaddy Map
//
//  Created by AHTHITHAN SELVANAYAGAM on 11/05/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/*  This view controller is used to display the stores in
    a particular category */
import UIKit
import FirebaseDatabase

class ShopViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    
    //@IBOutlet weak var myTextField: UITextField!
    @IBOutlet weak var myTableView: UITableView!
    
    // Initialize the variables.
    var myList:[String] = []
    var passedCategory = ""
    var shopToPass = ""
    
    var handle:FIRDatabaseHandle?
    
    var ref:FIRDatabaseReference?
    
    // Setting up the Table View for the application
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myList.count
    }
    
    //Data from the database is entered into each row of the table view.
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "cell")
        cell.textLabel?.text = myList[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.textLabel?.textColor = UIColor(red: 0.149, green: 0.3608, blue: 0.6196, alpha: 1.0)
        return cell
    }
    
    //When a category is selected a segue is performed to go to the next page
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Get Cell Label
        let currentCell = tableView.cellForRow(at: indexPath) as UITableViewCell!;
        shopToPass = (currentCell?.textLabel?.text)!
        performSegue(withIdentifier: "shopoutletsegue", sender: self)
    }
    
    //This method passes the value selected by the user to the ShopDetailsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "shopoutletsegue") {
            
            //New view controller initialized and casted as next view controller
            let viewController = segue.destination as! ShopDetailsViewController
            //New view controller with the property that stores the passed value
            viewController.passedCategory = passedCategory
            viewController.passedShop = shopToPass
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Access the database
        ref = FIRDatabase.database().reference()
        
        //Load the data from the database into the table view accordingly
        handle = ref?.child("shop").child(passedCategory).observe(.childAdded, with: { (snapshot) in
            
            if let item = snapshot.key as? String
            {
                self.myList.append(item)
                self.myTableView.reloadData()
            }
        })
        // Customizing the navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    
}

