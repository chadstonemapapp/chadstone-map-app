//
//  TutorialVideoViewController.swift
//  Chaddy Map
//
//  Created by Shanya & Ahthithan on 30/04/2017.
//  Copyright © 2017 chaddyapp. All rights reserved.
//
/* This view controller displays the tutorial 
 on how to use this app */

/* It includes a button which dispays a video tutorial if the
 user requires more assistance in how the app works */

import Foundation
import UIKit
import UIKit
import UIKit
import AVKit
import AVFoundation
import MessageUI


class TutorialVideoViewController: UIViewController {
    
    //Declearing the outlets for the page
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var menu: UIBarButtonItem!
    
    @IBOutlet weak var textbox2: UITextView!
    @IBOutlet weak var textbox1: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //This allows the menu button click to open the side bar menu        menu.target = self.revealViewController()
        menu.action = #selector(SWRevealViewController.revealToggle(_:))
        
        //Setting the background of the screen
        self.view.backgroundColor = UIColor.black
        
        //Gesture for swiping right and opening the side bar menu
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        
        //Customizing the objects in the screen
        videoButton.layer.cornerRadius = 10
        textbox1.layer.cornerRadius = 20
        textbox2.layer.cornerRadius = 20
        
        //Customizing the navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //This function performs error handling
    @IBAction func playMovie(_ sender: Any) {
        do {
            try playMovie()
        }
        catch {
            //Error message which is displayed if the video does not load
            debugPrint("Video Failed To Load!")
        }
    }
   
    
    
    
    //This function initalizes the attributes needed to play the video
    fileprivate func playMovie()
        throws {
            //Defining the path of the video
            let path = Bundle.main.path(forResource: "tutorial", ofType:"mov")
            //Defining the path of the player
            let videoPlayer = AVPlayer(url: URL(fileURLWithPath: path!))
            //Initializing the AVPlayer
            let Controller = AVPlayerViewController()
            Controller.player = videoPlayer
            //This defines the window of the video
            self.present(Controller, animated: true) {
                videoPlayer.play()
            }
    }
    
    
    
}

